package ru.antara.pages;

import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.$$;;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.CollectionCondition.itemWithText;


public class ResultPage {
    private final ElementsCollection results = $$(byXpath("//div[contains(@class,'search_results')]/div[contains(@class,'element')]//p[@class='name']/a"));

    public ResultPage shouldHaveAtTop(String name){
        results.first().shouldHave(text(name));
        return this;
    }

    public ResultPage shouldHaveSizeAndText(int size, String name) {
        results.shouldHave(sizeGreaterThan(size), itemWithText(name));
        return this;
    }
}
