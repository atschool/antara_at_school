package ru.antara.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;


public class SearchPage {
    private final SelenideElement searchForm = $("#formSearchMain");
    private final SelenideElement textArea = searchForm.find("input[name='m_act[find]']");
    private final SelenideElement genreSelect = searchForm.find("select[name='m_act[genre][]']");
    private final SelenideElement countrySelect = searchForm.find("select[name='m_act[country]']");
    private final SelenideElement searchButton = searchForm.find("input[value='поиск']");

    public SearchPage open() {
        Selenide.open("/");
        return this;
    }

    public SearchPage inputSearch(String searchString) {
        textArea.sendKeys(searchString);
        return this;
    }

    public SearchPage setGenre(String ...genres) {

        genreSelect.selectOption(genres);
        return this;
    }

    public SearchPage seCountry(String country) {
        countrySelect.selectOption(country);
        return this;
    }

    public void search(){
        searchButton.click();
    }
}
