import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.NoSuchElementException;



public class Timeouts {
    static {
        System.setProperty("webdriver.chrome.driver", "day08_selenium/src/main/driver/chromedriver/chromedriver.exe");
    }

    static WebDriver driver = new ChromeDriver();
    static WebElement params;

    public static void main(String[] args) {
        try {
            Thread.sleep(3_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.manage().window().maximize();

//        javaWait();
//        implicitlyWait();

        //explicitly wait
        webDriverWait();
        //fluentWait();

        untilContains();


        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.close();
    }

    private static void javaWait() {
        driver.get("https://ya.ru/");

        //TODO: Механизм ожидания предоставляемый Java
        try {
            Thread.sleep(7_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        params = driver.findElement(By.cssSelector("#text"));
        params.click();
        params.sendKeys("123");

    }

    private static void implicitlyWait() {
        driver.get("https://ya.ru/");

        //implicitly wait неявное ожидать указанное время прежде, чем выбросить NoSuchElementException
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        params = driver.findElement(By.cssSelector("#text"));
        params.click();
        params.sendKeys("123");

        //сбросим время неявного ожидания
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(0));

    }

    private static void webDriverWait() {
        driver.get("https://ya.ru/");

        // Explicit wait Явное ожидания определенного условия, которое должно быть выполнено прежде, чем тест пойдет дальше.
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        params = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#text")));

        params.click();
        params.sendKeys("123");

    }

    private static void fluentWait() {
        driver.get("https://www.kinopoisk.ru/s/");

        //fluent wait
        Wait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);
        params = fluentWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#find_film")));

        params.click();
    }

    private static void untilContains() {
        driver.get("https://www.kinopoisk.ru/s/");

        // scroll
        WebElement someElement = driver.findElement(By.xpath("//input[@id='find_studio']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", someElement);

        //webdriver wait
        //wait.until(d -> this.locationRegionList.findElement(By.xpath("./li")).getText().contains(location));
        // this.locationRegionList.findElement(By.xpath("./li")).click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        if( wait.until(ExpectedConditions.textToBePresentInElement(someElement, "123")) ) {
            params.clear();
        }
    }
}
