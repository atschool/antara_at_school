import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static com.google.gson.JsonParser.parseString;

import com.google.gson.JsonObject;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.stream.Stream;

class SwaggerPetApiTest {

    private static ArrayList<String> jsonsGetter(String path, String regex) {
        ArrayList<String> jsons = new ArrayList<>();
        File[] files = new File(path).listFiles(
                (dir, name) -> Pattern.compile(regex).matcher(name).find());
        JsonObject json;
        String petString;
        for(File file : files) {
            System.out.println("file is: " + file);
            try{
                petString = new String(Files.readAllBytes(file.toPath()));
                json = parseString(petString).getAsJsonObject();
                jsons.add(json.toString());
            }catch(IOException e){
                e.printStackTrace();
            }

        }
        return jsons;
    }

    private static Stream<Arguments> provider(String path, String regex) {
        ArrayList<String> jsons = jsonsGetter(path, regex);
        return jsons.stream().map(Arguments::of);
    }

    private static Stream<Arguments> postPetsProvider() {
        return provider("src/main/resources/", "pet\\d*_post.json");
    }

    private static Stream<Arguments> wrongUrlGetProvider() {
        return Stream.of(
                Arguments.of("https://petstore.swagger.io/v2/pet/qwe"),
                Arguments.of("https://petstore.swagger.io/v2/pet/922337203685477580800"),
                Arguments.of("https://petstore.swagger.io/v2/pet/0x100"),
                Arguments.of("https://petstore.swagger.io/v2/pet/10.12"),
                Arguments.of("https://petstore.swagger.io/v2/pet/%$#@!@")
        );
    }

    @BeforeAll
    static void setUp() {
        ArrayList<String> jsons = jsonsGetter("src/main/resources/initial/","init\\d*_delete.json" );
        for (String json : jsons) {
            given().contentType("application/json").body(json).
                    when().post("https://petstore.swagger.io/v2/pet");
        }
    }

    @ParameterizedTest
    @MethodSource("wrongUrlGetProvider")
    void wrongGetPet(String url) {
        given().log().body().
                when().get(url).
                then().log().body().assertThat().body("code", oneOf(400, 404));
    }

    @ParameterizedTest
    @MethodSource("postPetsProvider")
    void postPet(String petJson) {
        given().contentType("application/json").body(petJson).
        when().log().body().post("https://petstore.swagger.io/v2/pet").
                then().log().body().assertThat().statusCode(HttpStatus.SC_OK).body(equalTo(petJson));
    }

    @Test
    void petDelete() {
        when().delete("https://petstore.swagger.io/v2/pet/" + 40).then().log().body().assertThat().statusCode(200);
    }
}
