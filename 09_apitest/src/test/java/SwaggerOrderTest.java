import java.util.Random;

import static io.restassured.RestAssured.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;

import order.Endpoints;
import order.Order;

class SwaggerOrderTest {

    @BeforeAll
    static void setUp() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(Endpoints.host + Endpoints.orderBasePath) // задаём базовый адрес каждого ресурса
                .setAccept(ContentType.JSON) // задаём заголовок accept // приходит
                .setContentType(ContentType.JSON) // задаём заголовок content-type // когда мы отпр
                .log(LogDetail.ALL) // дополнительная инструкция полного логгирования для RestAssured
                .build(); // после этой команды происходит формирование стандартной "шапки" запроса.

        //Здесь задаётся фильтр, позволяющий выводить содержание ответа,
        // также к нему можно задать условия в параметрах конструктора, которм должен удовлетворять ответ (например код ответа)
        RestAssured.filters(new ResponseLoggingFilter());
    }

    //Для выбора рандомного значения
    private Order dummyOrder() {
        Order order = new Order();
        order.setPetId(new Random().nextInt(10000));
        // order.setId(new Random().nextInt(999999));
        order.setId(new Random().nextInt(100));
        order.setQuantity(new Random().nextInt(10));
        return order;
    }

    private void createOrder(Order order) {
        // given().spec(requestSpec) - можно использовать для конкретного запроса свою спецификацию
        given()
                .body(order)
                .when()
                .post(Endpoints.order)
                .then()
                .statusCode(200);
    }

    private Order getOrder(int id) {
        return given().pathParam("id", id)
                .when().get(Endpoints.order + Endpoints.orderId)
                .then().assertThat().
                statusCode(200).extract().body().as(Order.class);
    }

    @Test
    void createOrderTest() {
        Order order = dummyOrder();
        // создаем заказ
        createOrder(order);
        // проверяем правильно ли он создался
        Order apiOrder = getOrder(order.getId());
        assertEquals(order, apiOrder);
    }

    @Test
    void orderGetTest() {
        Order order = dummyOrder();
        // готовим данные для теста
        createOrder(order);
        // проверяем получение заказа
        Order apiOrder = getOrder(order.getId());
        assertEquals(order, apiOrder);
    }
}
