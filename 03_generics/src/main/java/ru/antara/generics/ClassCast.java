package ru.antara.generics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ClassCast {
    private Object id;
    private int sum;

    public static void main(String[] args) {

        ClassCast acc1 = new ClassCast("2334", 5000); // id - число
        int acc1Id = (int)acc1.getId();
        System.out.println(acc1Id);
    }
}
