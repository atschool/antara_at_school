package ru.antara.jdbc;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

/**
 * @author Shchepetkov
 * @since 1
 */

public class Main {

    //путь к нашему файлу конфигураций
    private static final String PATH_TO_PROPERTIES = "05_sql/src/main/resources/config.properties";

    //запрос
    private static final String QUERY = "SELECT * from LANIT.CITY";

    public static void main(String[] args) {

        FileInputStream fileInputStream;
        //инициализируем специальный объект Properties
        //типа Hashtable для удобной работы с данными
        Properties prop = new Properties();

        try {
            //обращаемся к файлу и получаем данные
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);

            //подключение
            Connection myConnection = DriverManager.getConnection(prop.getProperty("dbUrl"), prop.getProperty("userName"), prop.getProperty("password"));
            Statement sqlStatement = myConnection.createStatement();
            ResultSet myResultSet = sqlStatement.executeQuery(QUERY);
            while (myResultSet.next()) {
                System.out.println("Result: " + myResultSet.getString("ID_CITY") + " " + myResultSet.getString("NAME"));
            }
            myResultSet.close();
            myConnection.close();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
}
